#ifndef __HEADSTORE_H__
#define __HEADSTORE_H__

#include <string>

namespace HeadStore {

const char *loadFile(const std::string &file);

ulong getSize(const std::string &file);
}  // namespace HeadStore

#endif
