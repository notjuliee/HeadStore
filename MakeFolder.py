#!/usr/bin/env python3

#  HeadStore - Bundle compressed binary files into C++ programs
#
#  Written in 2018 by joonatoona
#
#  To the extent possible under law, the author(s) have dedicated all copyright
#  and related and neighboring rights to this software to the public domain
#  worldwide. This software is distributed without any warranty.
#
#  You should have received a copy of the CC0 Public Domain Dedication along
#  with this software. If not, see
#  <http://creativecommons.org/publicdomain/zero/1.0/>.

from subprocess import check_output as run
from sys import argv, exit
from glob import glob
from os import chdir, getcwd, path
from typing import List, Dict, Tuple

L = "{"
R = "}"
Q = "\""

if len(argv) < 2:
    print(f"Usage: {argv[0]} <path to folder>")
    exit(1)

C_DIR: str = getcwd()
S_DIR: str = path.dirname(path.realpath(__file__))
chdir(argv[1])
FILES: List[str] = glob("**/*", recursive=True)

print(f"Found {len(FILES)} files to store")

FILE_DATA: Dict[str, Tuple[int, str]] = {}

for f in FILES:
    if not path.isfile(f):
        continue
    fileData: Tuple[int, str] = (path.getsize(f) + 1, run([S_DIR + "/toarray", f]).decode())
    FILE_DATA[f] = fileData

chdir(C_DIR)

F_SIZE_CODE = ""
F_DATA_CODE = ""
for f in FILE_DATA:
    F_SIZE_CODE += f"{L} {Q}{f}{Q}, {FILE_DATA[f][0]} {R}, "
    F_DATA_CODE += f"{L} {Q}{f}{Q}, {L}{FILE_DATA[f][1]}{R} {R}, "

F_SIZE_CODE = F_SIZE_CODE[:-2]
F_DATA_CODE = F_DATA_CODE[:-2]


C_CODE = f"""
#include "HeadStore.h"

#include <zlib.h>
#include <unordered_map>
#include <vector>

const std::unordered_map<std::string, ulong> FILE_SIZE = {L} {F_SIZE_CODE} {R};
const std::unordered_map<std::string, std::vector<char>> FILE_DATA = {L} {F_DATA_CODE} {R};

"""+"""

const char *HeadStore::loadFile(const std::string &file) {
    char *buff = new char[FILE_SIZE.at(file)];
    ulong fsize = FILE_SIZE.at(file);
    uncompress((Bytef *)buff, &fsize, (Bytef *)&FILE_DATA.at(file)[0], (ulong)FILE_DATA.at(file).size());
    buff[FILE_SIZE.at(file) - 1] = '\\0';
    return buff;
}

ulong HeadStore::getSize(const std::string &file) {
    return FILE_SIZE.at(file);
}
"""[1:]

with open("HeadStore.cpp", "w+") as f:
    f.write(C_CODE)
