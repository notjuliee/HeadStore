# HeadStore

Bundle compressed binary files into C++ programs

# Requirements

The resulting source requires C++11 and zlib  
Generating the source requires python >3.5  

# Usage

First, you must compile `toarray.cpp`. On Linux, this is done using `$CXX -o toarray toarray.cpp $(pkg-config --cflags --libs zlib)`  
You can then generate `HeadStore.cpp` using `python3 MakeFolder.py <path to the folder you wish to bundle>`  

To use the generated code, make sure HeadStore.cpp is compiled and linked with your project.  
Example program to read a compressed file named `foo/bar.txt`:  
```
#include "HeadStore.h"

#include <iostream>

int main(int argc, char *argv[]) {
    const char* bar = HeadStore::loadFile("foo/bar.txt");
    std::cout << bar << std::endl;
    delete[] bar;
    return 0;
}
```

Note: you **MUST** remeber to delete[] the returned address to avoid leaking memory.


[![CC0](https://licensebuttons.net/p/zero/1.0/88x31.png)](https://creativecommons.org/publicdomain/zero/1.0/)
